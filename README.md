# TODO

1. ~~Update *bable-loader* from 6.24 to version 7.10+~~
2. ~~Update CSS to be able to work with SCSS~~
3. ~~Create grid system (Used Bootstrap)~~
4. Pages
    * ~~Header~~
    * ~~Hero~~
    * About Me
    * Experience
    * Project
    * Contact Me
    * Footer
5. Mobile-
    * Header
    * Hero 
    * Experience
    * Project
    * About Me
    * Contact Me
    * Footer
6. SendGrid Email.
7. Webpack production config? <br>
    https://webpack.js.org/guides/production/
8. Transform to TypsScript?

# Note to Myself
In package.json:
``` 
"heroku-prebuild": "echo This runs before Heroku installs your dependencies.",
"heroku-postbuild": "echo This runs afterwards."
```
Online working example
```
"heroku-postbuild": "webpack -p --config ./webpack.prod.config.js --progress"
```