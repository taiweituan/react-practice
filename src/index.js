import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";

const createStoreWithMiddleware = applyMiddleware()(createStore);



import Header from "./components/common/header";
import Hero from "./components/hero";
import Experience from "./components/experience";
import Aboutme from "./components/aboutme";
// import Projects from "./components/projects";
// import Contact from "./components/contact";

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <div className="">
            <Header />
            <Hero />
            <Aboutme />
            <Experience />
            {/* <Projects />
            <Contact /> */}
        </div>
    </Provider>
    , document.getElementById("index")
);