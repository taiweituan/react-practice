import React, { Component } from "react";

import JobList from "../container/job-list";
import JobDetails from "../container/job-details";
export default class Experience extends Component {
    render() {
        return (
            <div id="experience" className="experience">
                <div className="container">
                    <h2 className="section-header">Experinece</h2>
                    <div className="row">
                        <JobList/>
                        <JobDetails/>
                    </div>
                </div>
            </div>
        );
    }
}