import React, { Component } from "react";

export default class Aboutme extends Component {
    render() {
        return (
            <div id="about-me" className="about-me">
                <div className="container">
                    <h2 className="section-header">About me</h2>
                    <p>4 years of professional web development experience and still counting, along with Computer Science major as my programming foundation. Studied and work what I am passionated for. “Never stop learning” I like to learn from other experts and combine with my knowledge to write better, cleaner, more efficient code.</p>
                    
                </div>
                <div className="container">
                    <div className="card-deck">
                        <div className="card shadow-sm">
                            <div className="card-img-top">
                            </div>

                            <div className="card-body">
                                <h5 className="card-title">Professional</h5>
                                <p className="card-text">4 years of professional experiencein Web Development, with solid fundamental of programming from Computer Science graduate.</p>
                                <a className="btn btn-primary" href="#experience">Check Me Out</a>
                            </div>
                        </div>

                        <div className="card shadow-sm">
                            <div className="card-img-top">
                            </div>

                            <div className="card-body">
                                <h5 className="card-title">Passionated</h5>
                                <p className="card-text">4 years of professional experiencein Web Development, with solid fundamental of programming from Computer Science graduate.</p>
                                <a className="btn btn-primary" href="#experience">Check Me Out</a>
                            </div>
                        </div>

                        <div className="card shadow-sm">
                            <div className="card-img-top">
                            </div>

                            <div className="card-body">
                                <h5 className="card-title">Disciplined</h5>
                                <p className="card-text">4 years of professional experiencein Web Development, with solid fundamental of programming from Computer Science graduate.</p>
                                <a className="btn btn-primary" href="#experience">Check Me Out</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}