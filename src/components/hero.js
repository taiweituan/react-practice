import React, { Component } from "react";

export default class Hero extends Component {
    render () {
        return (
            <div className="hero">
                <div className="avatar">
                    <figure className="avatar__shape">
                        <img className="avatar__img" src="https://images.pexels.com/photos/416160/pexels-photo-416160.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Taiwei Tuan" />
                        <figcaption className="avatar__caption">
                            Hello!
                        </figcaption>
                    </figure>
                </div>
                <h1 className="hero__title">Hi, I am Taiwei!</h1>
                <h2 className="hero__title">A Professional Web Developer</h2>
            </div>
        );
    }
}