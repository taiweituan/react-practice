
export default function(){
    return [{
        title: "Web Developer",
        company: "HERE Technologies",
        startDate: {
            month: "May",
            year: "2016"
        },
        endDate:{
            month: "",
            year: ""
        },
        location: {
            city: "Chicago",
            state: "IL"
        },
        iconId: "icn-here-technologies"
    },{
        title: "Web Specialist",
        company: "LeadertechUSA",
        startDate: {
            month: "Jan",
            year: "2015"
        },
        endDate:{
            month: "Feb",
            year: "2016"
        },
        location: {
            city: "Wood Dale",
            state: "IL"
        },
        iconId: ""
    }];
}