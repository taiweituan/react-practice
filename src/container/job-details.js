import React, { Component } from "react";
import {connect} from "react-redux";

class JobDetail extends Component {

    render() {
        if (!this.props.job){
            return <div>Please make selection.</div>;
        }

        return (

            <div className="jobs__container col-md-9">
                <div className="jobs__icon-container">
                    <span className="jobs__icon" id={this.props.job.iconId}>&nbsp;</span>
                </div>
                <div className="jobs__details">
                    <h2 className="jobs__title">
                        {this.props.job.title}
                    </h2>
                    <h4 className="jobs__company-name">{this.props.job.company}</h4>
                    <div className="jobs__date-range">
                        {this.props.job.startDate.month}&nbsp;{this.props.job.startDate.year} --&nbsp;
                        {this.props.job.endDate.month}&nbsp;{this.props.job.endDate.year}
                    </div>

                    <div className="jobs__location">
                        {this.props.job.location.city}, {this.props.job.location.state}
                    </div>


                    <p className="jobs__description">
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloribus corrupti quisquam sit! Quis, commodi! Voluptate, impedit sint quisquam obcaecati ullam voluptatem ut, quos soluta, asperiores quis ad aperiam aspernatur fugiat.
                    </p>

                    
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        job: state.activeJob
    };
}

export default connect(mapStateToProps)(JobDetail);